**SDKs Used**

1. Alamofire.
2. SDWebImage/GIF (FLAnimatedImage).
3. Nimble for unit testing.

---

## Todos

1. SDWebImage take care of early feteching and Gif displaying but memory could handled better.
2. Reloading and fetching data enhancment. 
3. better error handling to the user.
4. UX, like pull to refresh, error notification rather than alert etc.. 

---

## Time

1. split between 2 days each around 2 hours in total around 4 hours + Pushing time :)
2. Mostly I take more time with the things I deal with for the first time like gify and gifs, for better code handling, testing, refactor and styling.
3. Mostly for such a task If I worked with it before it will take little less. :)

---

## How To run.

I pushed everything including the pods to the repo, so all it needs is to run directly, If something wrong happens please let me know.