//
//  TrendingRequest.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

struct TrendingRequest: Encodable {
    var offset: Int
}
