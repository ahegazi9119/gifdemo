//
//  TrendingResponse.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

struct TrendingResponse: Decodable {

    let pagination: Pagination
    let data: [Gif]

    struct Pagination: Decodable {
        let count: Int
        let offset: Int
    }

}
