//
//  Image.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

struct Gif: Decodable {

    let id: String
    let downsized: Image
    let original: Image

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        let imageContainer = try container.nestedContainer(keyedBy: CodingKeys.Images.self, forKey: .images)
        downsized = try imageContainer.decode(Image.self, forKey: .downsized)
        original = try imageContainer.decode(Image.self, forKey: .original)
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case images

         enum Images: String, CodingKey {
            case downsized = "fixed_height_small"
            case original = "original"
        }
    }

    struct Image: Decodable {
        let url: String
    }

}
