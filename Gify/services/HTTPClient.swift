//
//  HTTPClient.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation
import Alamofire

protocol HTTPClient {
    func isInternetConnectionAvailable() -> Bool
    func get<T: Encodable, U: Decodable>(urlString: String, headers: [String: String]?, parameters: T?, onSuccess: @escaping (U) -> Void, onFailure: @escaping (Error) -> Void)
}

final class HTTPService: HTTPClient {

    func isInternetConnectionAvailable() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }

    func get<T: Encodable, U: Decodable>(urlString: String, headers: [String: String]?, parameters: T?, onSuccess: @escaping (U) -> Void, onFailure: @escaping (Error) -> Void) {
        guard isInternetConnectionAvailable() else {
            onFailure(NetworkError.noInternetConnection)
            return
        }

        do {
            let url = try requestURL(from: urlString)
            let request = try jsonRequest(from: parameters)
            Alamofire.request(url, method: .get, parameters: request, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
                switch response.result {
                case .success(let value):
                    do {
                        let decoder = JSONDecoder()
                        onSuccess(try decoder.decode(U.self, from: value))
                    } catch {
                        onFailure(NetworkError.invalidData)
                    }
                case .failure(let error):
                    onFailure(error)
                }
            }
        } catch {
            onFailure(error)
        }
    }

}

private extension HTTPService {

    func requestURL(from urlString: String) throws -> URL {
        guard let requestURL = URL(string: urlString) else {
            throw NetworkError.invalidURL
        }
        return requestURL
    }

    func jsonRequest<T: Encodable>(from parameters: T?) throws -> [String: Any]? {
        guard parameters != nil else { return nil }
        let jsonEncoder = JSONEncoder()
        let json = try jsonEncoder.encode(parameters)
        guard let jsonRequest = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String: Any] else {
            throw NetworkError.invalidParameters
        }
        return jsonRequest
    }

}
