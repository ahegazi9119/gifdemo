//
//  GifsHttpClient.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

protocol GifsHttpClient {
    func getTrendingGifs(request: TrendingRequest, onSuccess: @escaping (_ response: TrendingResponse) -> Void, onFailure: @escaping (_ error: Error) -> Void)
}

final class GifsHttpService: GifsHttpClient {

    private let httpClient: HTTPClient

    init(httpClient: HTTPClient = HTTPService()) {
        self.httpClient = httpClient
    }

    func getTrendingGifs(request: TrendingRequest, onSuccess: @escaping (_ response: TrendingResponse) -> Void, onFailure: @escaping (_ error: Error) -> Void) {

        // there is something wrong with alamofire query parm so I did it this way because of the challenge.
        // left the request in the signature etc. to show how it should to be :)
        let url = "\(Url.trending.rawValue)\(request.offset)"
        httpClient.get(urlString: url, headers: nil, parameters: request, onSuccess: onSuccess, onFailure: onFailure)
    }

}
