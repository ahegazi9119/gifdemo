//
//  Constants.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case invalidParameters
    case invalidData
    case noInternetConnection
}

enum ApplicationKey: String {
    case gify = "YmxzxSywM15Q1hLMDw33Wp2edQyWAtg3"
}

enum Url: String {
    case trending = "https://api.giphy.com/v1/gifs/trending?api_key=YmxzxSywM15Q1hLMDw33Wp2edQyWAtg3&offset="
}

enum CollectionViewCell: String {
    case gifCollectionViewCell = "GifCollectionViewCell"
}

enum ViewController: String {
    case gifViewController = "GifViewController" 
}
