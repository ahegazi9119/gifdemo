//
//  GifsCollectionPresenter.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

protocol GifsBrowserPresenterable {
    var delegate: GifsBrowserPresenterDelegate? { get set }
    func requestGifs()
}

protocol GifsBrowserPresenterDelegate {
    func didRetriveGifs(gifs: [Gif])
    func didFailedRetriveGifs(with error: Error)
}

final class GifsBrowserPresenter: GifsBrowserPresenterable {

    private let gifsClient: GifsHttpClient

    var delegate: GifsBrowserPresenterDelegate?

    init(gifsClient: GifsHttpClient = GifsHttpService()) {
        self.gifsClient = gifsClient
    }

    func requestGifs() {
        let offset = 25
        let _ = DispatchQueue.global(qos: .background)
        DispatchQueue.concurrentPerform(iterations: 20) { (i) in
            requestGifs(offset: offset * (i + 1))
        }
    }

}

private extension GifsBrowserPresenter {

    func requestGifs(offset: Int) {
        gifsClient.getTrendingGifs(request: TrendingRequest(offset: offset), onSuccess: { [weak self] response in
            self?.delegate?.didRetriveGifs(gifs: response.data)
        }) { [weak self] error in
            DispatchQueue.main.async { [weak self] in 
                self?.delegate?.didFailedRetriveGifs(with: error)
            }
        }
    }

}

