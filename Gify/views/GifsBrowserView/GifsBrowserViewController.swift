//
//  GifsCollectionViewController.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import UIKit
import FLAnimatedImage
import SDWebImage

class GifsBrowserViewController: UIViewController, UICollectionViewDelegate {

    @IBOutlet private weak var collectionView: UICollectionView!
    private var presenter: GifsBrowserPresenterable!
    private var gifs: [Gif] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        initPresenter()
        collectionView.isPrefetchingEnabled = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.requestGifs()
    }

}

private extension GifsBrowserViewController {

    func initPresenter() {
        presenter = GifsBrowserPresenter()
        presenter.delegate = self
    }

}

extension GifsBrowserViewController: GifsBrowserPresenterDelegate {

    func didRetriveGifs(gifs: [Gif]) {
        self.gifs.append(contentsOf: gifs)
        // reload data when done, if any failure happen,
        // failed method will be called and reload the data with what it has.
        // this should be made in a better way than validationg static number.
        // there is an issue with animating gifs, so made it this way for now.
        guard self.gifs.count == 500 else { return }
        collectionView.reloadData()
    }

    func didFailedRetriveGifs(with error: Error) {
        collectionView.reloadData()
        guard (error as? NetworkError) == NetworkError.noInternetConnection else {
            presentAlert(with: "We are sorry something went wrong please try again later.")
            return
        }

        presentAlert(with: "There is no ineternect connection please check it.")
    }

    private func presentAlert(with message: String) {
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: UIAlertControllerStyle.alert)

        let okayAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) { _ in
            alert.dismiss(animated: true, completion: nil)
        }

        alert.addAction(okayAction)

        present(alert, animated: true, completion: nil)
    }

}

extension GifsBrowserViewController:  UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gifs.count
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: CollectionViewCell.gifCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CollectionViewCell.gifCollectionViewCell.rawValue)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.gifCollectionViewCell.rawValue, for: indexPath) as! GifCollectionViewCell
        cell.setGif(with: gifs[indexPath.row].downsized.url)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        (cell as? GifCollectionViewCell)?.stopAnimating()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let gifCell = (cell as? GifCollectionViewCell) else { return }
        gifCell.setGif(with: gifs[indexPath.row].downsized.url)
        gifCell.startAnimating()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = storyboard?.instantiateViewController(withIdentifier: ViewController.gifViewController.rawValue) as? GifViewController

        guard let gifViewController = viewController else { return }

        gifViewController.gif = gifs[indexPath.row]
        navigationController?.pushViewController(gifViewController, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: ((view.frame.size.width / 2.0) - 2.0), height: ((view.frame.size.width / 2.0) - 2.0))
    }

}

