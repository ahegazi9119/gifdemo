//
//  GifCollectionViewCell.swift
//  Gify
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import UIKit
import FLAnimatedImage
import SDWebImage

class GifCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var gifImageView: FLAnimatedImageView!

    func startAnimating() {
        gifImageView.startAnimating()
    }

    func stopAnimating() {
        gifImageView.stopAnimating()
    }

    func setGif(with url: String) {
        guard let url = URL(string: url) else {
            // todo.
            // handle if the url is wrong.
            return
        }

        gifImageView.sd_setImage(with: url, placeholderImage: nil)
    }

}
