//
//  GifViewController.swift
//  Gify
//
//  Created by ahmed hegazi on 7/22/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import UIKit
import FLAnimatedImage
import SDWebImage

class GifViewController: UIViewController {

    @IBOutlet weak var activityIndecator: UIActivityIndicatorView!
    @IBOutlet private weak var imageView: FLAnimatedImageView!
    var gif: Gif!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadGif()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func loadGif() {
        guard let url = URL(string: gif.original.url) else {
            // handle error.
            return
        }

        activityIndecator.startAnimating()
        imageView.sd_setImage(with: url) { [weak self] (_, error, _, _) in
            self?.activityIndecator.stopAnimating()
            guard error != nil else {
                // handle error.
                return
            }
        }
    }

}
