//
//  MockedGifsHttpClient.swift
//  GifyTests
//
//  Created by ahmed hegazi on 7/22/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

final class MockedGifsHttpClient: GifsHttpClient {

    var shouldFail = false

    func getTrendingGifs(request: TrendingRequest, onSuccess: @escaping (TrendingResponse) -> Void, onFailure: @escaping (Error) -> Void) {

        guard shouldFail else {
            onSuccess(TrendingResponse(pagination: TrendingResponse.Pagination(count: 1, offset: 0), data: [Gif(id: "THIS_IS_ID", downsized: Gif.Image(url: "https://"), original: Gif.Image(url: "https://"))]))
            return
        }

        onFailure(NetworkError.noInternetConnection)
    }

}
