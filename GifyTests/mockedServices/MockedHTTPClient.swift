//
//  MockedHTTPClient.swift
//  GifyTests
//
//  Created by ahmed hegazi on 7/22/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import Foundation

final class MockedHTTPService: HTTPClient {

    var isNoInternectConnection = false

    func isInternetConnectionAvailable() -> Bool {
        return !isNoInternectConnection
    }

    func get<T, U>(urlString: String, headers: [String : String]?, parameters: T?, onSuccess: @escaping (U) -> Void, onFailure: @escaping (Error) -> Void) where T : Encodable, U : Decodable {

        guard isNoInternectConnection else {
            onSuccess(getReponse() as! U)
            return
        }

        onFailure(NetworkError.noInternetConnection)
    }

    private func getReponse() -> TrendingResponse {
        return TrendingResponse(pagination: TrendingResponse.Pagination(count: 1, offset: 0), data: [Gif(id: "1234", downsized: Gif.Image(url: "https://"), original: Gif.Image(url: "https://"))])
    }

}

struct Gif: Decodable {

    let id: String
    let downsized: Image
    let original: Image

    struct Image: Decodable {
        let url: String
    }
}
