//
//  GifsBrowserTests.swift
//  GifyTests
//
//  Created by ahmed hegazi on 7/22/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import XCTest
import Nimble

@testable import Gify

final class GifsBrowserTests: XCTestCase {

    let client = MockedGifsHttpClient()
    var sut: GifsBrowserPresenterable!
    var delegate = MockedGifPresenterDelegate()
    override func setUp() {
        super.setUp()
        sut = GifsBrowserPresenter(gifsClient: client)
        sut.delegate = delegate
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testRequestGifsWithNoInternetConnectionShouldReturnError() {
        client.shouldFail = true
        sut.requestGifs()
        expect((self.delegate.error as? NetworkError)).toEventually(equal(NetworkError.noInternetConnection))
    }

    func testRequestGifShouldSuccess() {
        client.shouldFail = false
        sut.requestGifs()
        expect((self.delegate.gifs.count)).toEventually(equal(20))
    }

}

class MockedGifPresenterDelegate: GifsBrowserPresenterDelegate {

    var gifs: [Gif] = []
    var error: Error?

    func didRetriveGifs(gifs: [Gif]) {
        print(gifs)
        self.gifs.append(Gif(id: "this is an id", downsized: Gif.Image(url: "http://"), original: Gif.Image(url: "http://")))
    }

    func didFailedRetriveGifs(with error: Error) {
        self.error = error
    }

}
