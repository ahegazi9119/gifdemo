//
//  GifyTests.swift
//  GifyTests
//
//  Created by ahmed hegazi on 7/21/18.
//  Copyright © 2018 com.ahmed.hegazi. All rights reserved.
//

import XCTest
import Nimble

@testable import Gify

final class GifsHttpClientTests: XCTestCase {

    let httpClient = MockedHTTPService()
    var sut: GifsHttpClient!
    
    override func setUp() {
        super.setUp()
        sut = GifsHttpService(httpClient: httpClient)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testRequestTrendingWithNoInternetConnectionShouldFail() {
        httpClient.isNoInternectConnection = true
        sut.getTrendingGifs(request: TrendingRequest(offset: 0), onSuccess: { response in
        }) { error in
            expect((error as! NetworkError)).toEventually(equal(NetworkError.noInternetConnection))
        }
    }

    func testRequestTrendingWithInternetConnectionShouldSuccessed() {
        httpClient.isNoInternectConnection = false
        sut.getTrendingGifs(request: TrendingRequest(offset: 0), onSuccess: { response in
            expect(response.data.count).toEventually(equal(1))
        }) { error in

        }
    }
    
}
